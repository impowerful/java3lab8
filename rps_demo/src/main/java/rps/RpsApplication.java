//Yu Hua Yang 2133677
package rps;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
    private RpsGame rps = new RpsGame();

    public void start(Stage stage) {
        Group root = new Group();

        //scene is associated with the container, and its dimensions
        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.BLACK);

        Button b1 = new Button("Rock");
        Button b2 = new Button("Paper");
        Button b3 = new Button("Scissors");
        HBox buttons = new HBox();
        buttons.getChildren().addAll(b1, b2, b3);

        TextField tInfo = new TextField("Welcome!");
        tInfo.setPrefWidth(200);
        TextField tWins = new TextField("Wins: 0");
        TextField tLosses = new TextField("Losses: 0");
        TextField tTies = new TextField("Ties: 0");
        HBox textFields = new HBox();
        textFields.getChildren().addAll(tInfo, tWins, tLosses, tTies);

        VBox v = new VBox();
        v.getChildren().addAll(buttons, textFields);

        root.getChildren().add(v);

        b1.setOnAction(new RpsChoice(tInfo, tWins, tLosses, tTies, "rock", rps));
        b2.setOnAction(new RpsChoice(tInfo, tWins, tLosses, tTies, "paper", rps));
        b3.setOnAction(new RpsChoice(tInfo, tWins, tLosses, tTies, "scissors", rps));
        //associate scene to stage and show
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);

        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}

