//Yu Hua Yang 2133677
package rps;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rng;

    public RpsGame(int wins, int ties, int losses) {
        this.wins = wins;
        this.ties = ties;
        this.losses = losses;
        this.rng = new Random();
    }

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.rng = new Random();
    }

    public int getWins() {
        return this.wins;
    }
    public int getTies() {
        return this.ties;
    }
    public int getLosses() {
        return this.losses;
    }

    public String playRound(String s) {
        //rock(0), paper(1), scissors(2)
        int newRng = this.rng.nextInt(3);
        String result = "";
        String comp = "";
        //draw
        if ((s.equals("rock") && newRng == 0) || (s.equals("paper") && newRng == 1) || (s.equals("scissors") && newRng == 2)) {
            this.ties++;
            result = "ties";
        }//player wins
        else if ((s.equals("rock") && newRng == 2) || (s.equals("paper") && newRng == 0) || (s.equals("scissors") && newRng == 1)) {
            this.wins++;
            result = "loses"; //comp loses if player wins
        }//computer wins
        else if ((s.equals("rock") && newRng == 1) || (s.equals("paper") && newRng == 2) || (s.equals("scissors") && newRng == 0)) {
            this.losses++;
            result = "wins"; //comp wins if player loses
        }

        switch (newRng) {
            case 0:
                comp = "rock";
                break;
            case 1:
                comp = "paper";
                break;
            case 2:
                comp = "scissors";
                break;
        }
        return "Computer plays " + comp +" and it " + result;
    }
}
