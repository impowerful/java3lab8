//Yu Hua Yang 2133677
package rps;

public class App 
{
    public static void main( String[] args )
    {
        RpsGame rp = new RpsGame();

        System.out.println(rp.playRound("rock"));
        System.out.println(rp.playRound("paper"));
        System.out.println(rp.playRound("scissors"));
        System.out.println(rp.playRound("rock"));
        System.out.println(rp.playRound("paper"));
        System.out.println(rp.playRound("scissors"));

        System.out.println(rp.getWins());
        System.out.println(rp.getTies());
        System.out.println(rp.getLosses());
    }
}