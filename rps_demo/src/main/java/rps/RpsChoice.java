//Yu Hua Yang 2133677
package rps;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField tI;
    private TextField tW;
    private TextField tL;
    private TextField tT;
    private String userChoice;
    private RpsGame rps;

    public RpsChoice(TextField tI, TextField tW, TextField tL, TextField tT, String userChoice, RpsGame rps) {
        this.tI = tI;
        this.tW = tW;
        this.tL = tL;
        this.tT = tT;
        this.userChoice = userChoice;
        this.rps = rps;
    }

    @Override
    public void handle(ActionEvent event) {
        String result = rps.playRound(userChoice);
        tI.setText(result);

        tW.setText("Wins: " + rps.getWins());
        tL.setText("Losses: " + rps.getLosses());
        tT.setText("Ties: " + rps.getTies());
    }
    
}